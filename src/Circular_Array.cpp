#include <iostream>
#include "Circular_Array.h"

Circular_Array::Circular_Array(int init_size)
{
    nums = new int[init_size];
    length = init_size;
}

Circular_Array::Circular_Array(int* init_arr, int init_size)
{
    nums = init_arr;
    length = init_size;
}

Circular_Array::~Circular_Array()
{
    //dtor
}

int &Circular_Array::operator[](int index)
{
    return nums[index % length];
}

Circular_Array* Circular_Array::get_copy()
{
    Circular_Array* clone = new Circular_Array(this->length);
    for (int i = 0; i < this->length; i++) {
        (*clone)[i] = this->nums[i];
    }
    return clone;
}
