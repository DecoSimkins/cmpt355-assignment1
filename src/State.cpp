#include "State.h"
#include <iostream>
#include <sstream>

int* State::solution = NULL;

State::State(Circular_Array* init_small_disks)
{
    this->small_disks = init_small_disks;
    this->cost = 0;
    this->evaluation = 0;
    this->heuristic = 0;
    this->parent = NULL;
}

State::~State()
{
    //dtor
}

void State::Evaluate(State* s)
{
	s->set_heuristic(State::Heuristic(s));
	if (s->get_parent() != NULL)
		s->set_cost(s->get_parent()->get_eval());
	s->set_evaluation(s->get_heuristic() + s->get_cost());
}

unsigned short State::Heuristic(State* s)
{
	unsigned short misplaced = 0;
	for (int i = 0; i < s->get_small_disks().get_length(); i++) {
		if (s->get_small_disks()[i] != State::solution[i])
			misplaced++;
	}
	return misplaced;
}

void State::printState()
{
	Circular_Array* disks = this->small_disks;
	for (int i = 0; i < this->small_disks->get_length(); i++) {
		std::cout << (*disks)[i] << ' ';
	}
	std::cout << "H: " << this->heuristic << " C: " << this->cost << " E: " << this->evaluation;
	std::cout << std::endl;
}

std::string *State::asString()
{
	std::string *state_string;
	std::ostringstream ss;
	for (int i = 0; i < this->small_disks->get_length(); i++) {
		ss << (*this->small_disks)[i];
	}
	state_string = new std::string(ss.str());
	return state_string;
}