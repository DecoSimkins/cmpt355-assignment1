#include <iostream>

#include "Frontier_Manager.h"
#include "state_queue.h"

Frontier_Manager::Frontier_Manager(state_queue* init_frontier, Circular_Array* init_big_disks)
{
    this->frontier = init_frontier;
    this->big_disks = init_big_disks;
    this->closed = new std::unordered_map<std::string, State*>();
}

Frontier_Manager::~Frontier_Manager()
{
    delete this->closed;
}

std::vector <int>* Frontier_Manager::gen_valid_indeces(int empty_index)
{
    std::vector <int>* valid_indeces = new std::vector<int>();
    int big_disk_num = (*big_disks)[empty_index];
    valid_indeces->push_back(empty_index + big_disk_num);
    valid_indeces->push_back(empty_index - big_disk_num);
    if ((*big_disks)[empty_index] != 1) {
        valid_indeces->push_back(empty_index + 1);
        valid_indeces->push_back(empty_index - 1);
    }

    return valid_indeces;
}


void Frontier_Manager::generate_states(State* s)
{
    Circular_Array small_disks = s->get_small_disks();
    int empty_disk_index = find_empty_disk_index(*s);
    std::vector <int>* valid_indeces = gen_valid_indeces(empty_disk_index);

    for (unsigned int i = 0; i < valid_indeces->size(); i++) {
        Circular_Array* temp_arr = s->get_small_disks().get_copy();
        (*temp_arr)[empty_disk_index] = (*temp_arr)[(*valid_indeces)[i]];
        (*temp_arr)[(*valid_indeces)[i]] = 0;
		

		State* tempState = new State(temp_arr);
		if (this->closed->count((*tempState->asString())) == 0) {
			tempState->set_parent(s);
			State::Evaluate(tempState);
        	frontier->push(tempState);
		} else {
			delete tempState;
			delete temp_arr;
		}
    }
}

int Frontier_Manager::find_empty_disk_index(State s)
{
    Circular_Array small_disks = s.get_small_disks();
    for (int i = 0; i < small_disks.get_length(); i++) {
        if (small_disks[i] == 0) {
            return i;
        }
    }
    return 0;
}

void Frontier_Manager::close(State* toClose)
{
	std::string *state_string = toClose->asString();
	(*this->closed)[*state_string] = toClose;
}

