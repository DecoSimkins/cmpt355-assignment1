CC = g++
CFLAGS = -g -Wall

SOURCEEXTENSION = cpp

SOURCEF = ./src
INCLUDEF = ./include
BINF = ./bin
OUTF = ./out

# Test Folders
TESTROOTF = ./test
TESTSOURCEF = $(TESTROOTF)/source
TESTINCLUDEF = $(TESTROOTF)/include
TESTBINF = $(TESTROOTF)/bin
TESTOUTF = $(TESTROOTF)/out

MAIN_OBJS = $(BINF)/Circular_Array.o $(BINF)/State.o $(BINF)/Frontier_Manager.o

.PHONY : setup clean
.SILENT : setup clean

AB : main.$(SOURCEEXTENSION) $(MAIN_OBJS)
	make setup
	$(CC) $(CFLAGS) -I$(INCLUDEF) -o $(OUTF)/$@ $^

main-debug : main.$(SOURCEEXTENSION) $(MAIN_OBJS)
	$(CC) $(CFLAGS) -I$(INCLUDEF) -g -o $(OUTF)/$@ $^

quick-run:
	@clear
	@make
	@./out/main 10 < AB10.txt

# Object rule
$(BINF)/%.o : $(SOURCEF)/%.$(SOURCEEXTENSION) $(INCLUDEF)/%.h
	$(CC) $(CFLAGS) -c -I$(INCLUDEF) -o $@ $<

setup :
	mkdir -p $(SOURCEF)
	mkdir -p $(INCLUDEF)
	mkdir -p $(BINF)
	mkdir -p $(OUTF)
	mkdir -p $(TESTROOTF)
	mkdir -p $(TESTSOURCEF)
	mkdir -p $(TESTINCLUDEF)
	mkdir -p $(TESTBINF)
	mkdir -p $(TESTOUTF)

clean :
	rm -f $(BINF)/*
	rm -f $(OUTF)/*
	rm -f $(TESTBINF)/*
	rm -f $(TESTOUTF)/*
