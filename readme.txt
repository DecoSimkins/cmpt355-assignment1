Directory Structure:
--------------------
The bin directory is where .o end up on compilation
The include directory is where header files are located
The out directory contains executables (notably, AB)
The src directory contains source files (.cpp)
The test directory contains this same set of directories but was used for tests
(of which there were few), and is now empty.

main.cpp is located in the root folder along with the Makefile, this readme,
and the sample inputs AB10.txt and AB17.txt

Code compilation execution:
---------------------------
Run 'make' to compile the program (this will create required directories,
as described above in addition to compiling the program)
The executable 'AB' will be located at './out/AB' (from the project's
root directory) and can be run as specified in the assignment spec;
i.e. './out/AB 10 < AB10.txt'
