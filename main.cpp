#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <queue>
#include <stack>

#include "Circular_Array.h"
#include "State.h"
#include "Frontier_Manager.h"
#include "state_queue.h"

int main(int argc, char* argv[])
{
    int size;
    std::istringstream(std::string(argv[1])) >> size;

    int largeDisk[size], smallDisk[size];
    int largeNumbers, smallNumbers;
    for (int i =0; i < size; i++){

        std::cin >> largeNumbers;

        largeDisk[i] = largeNumbers;
    }
    for (int i =0; i < size; i++){

        std::cin >> smallNumbers;

        smallDisk[i] = smallNumbers;
    }

    int n = sizeof(smallDisk) / sizeof(smallDisk[0]);

    std::vector<int> v (smallDisk, smallDisk + n);
    std::sort(v.begin(), v.end());

    int tempAnswer[size];
    std::copy(v.begin(), v.end(), tempAnswer);
    
    int temp = tempAnswer[0];
    for(int i = 0;i<=size-1;i++)
    {
        tempAnswer[i] = tempAnswer[i+1];
    }
    tempAnswer[size-1] = temp;
	State::solution = tempAnswer;

    Circular_Array* big_disks = new Circular_Array(largeDisk, size);
    Circular_Array* small_disks = new Circular_Array(smallDisk, size);

    State* test_state = new State(small_disks);
	State::Evaluate(test_state);
	state_queue* frontier = new state_queue;
	
    Frontier_Manager* fm = new Frontier_Manager(frontier, big_disks);
    fm->generate_states(test_state);
    
	State *nextState = test_state;
    int check = 0;
    while (nextState->get_heuristic() != 0){
        if (nextState == nullptr && frontier->empty() == true){
            check = 1;
            break;
        }else{
            nextState = frontier->top(); // access element
            frontier->pop(); // remove element
            if (nextState->get_heuristic() == 0) // solution
                break;
            fm->close(nextState); // add to closed
            fm->generate_states(nextState);
        }
    }

	if (check == 0){
        std::cout << "Solution Is:" << std::endl;
        std::stack<State*>* solution_stack = new std::stack<State*>();
        while (nextState != nullptr) {
            solution_stack->push(nextState);
            nextState = nextState->get_parent();
        }
        while (!solution_stack->empty()) {
            nextState = solution_stack->top();
            solution_stack->pop();
            for (int i = 0; i < nextState->get_small_disks().get_length(); i++) {
                std::cout << nextState->get_small_disks()[i] << ' ';
            }
            std::cout << std::endl;
        }
    }else{
        std::cout << "No Solution" << std::endl;
    }

    return 0;
}
