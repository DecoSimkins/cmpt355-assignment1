#ifndef CIRCULAR_ARRAY_H
#define CIRCULAR_ARRAY_H


class Circular_Array
{
    public:
        Circular_Array(int init_size);
        Circular_Array(int* init_arr, int init_size);
        virtual ~Circular_Array();
		// Loop back around to start of array for any index that is
		// out of bounds
        int& operator[](const int index);

        // Get
        int* get_nums() { return nums; }
        int get_length() { return length; }
        Circular_Array* get_copy();

    private:
        int* nums;
        int length;
};

#endif // CIRCULAR_ARRAY_H
