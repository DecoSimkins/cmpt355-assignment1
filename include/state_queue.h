#ifndef STATE_QUEUE_H
#define STATE_QUEUE_H

/* Used for standard library priority queue comparisons
 */ 
class state_comp 
{
	public:
		bool operator() (const State* s1, const State* s2)
		{
			return s1->get_eval() > s2->get_eval();
		}
};

typedef std::priority_queue<State*, std::vector<State*>, state_comp> state_queue;

#endif