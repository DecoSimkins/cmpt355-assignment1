#ifndef STATE_H
#define STATE_H

#include <string>

#include "Circular_Array.h"

// Maybe track empty disk index
class State
{
    public:
        State(int small_disks_size);
        State(Circular_Array* init_small_disks);
        virtual ~State();

		// Sets this states heuristic, cost, and evaluation
        static void Evaluate(State* s);
		// Calculates and returns this states herustic
		static unsigned short Heuristic(State* s);
		// Int array containing the solution
		static int* solution;

		// Output small disks followed by heuristic, cost and evaluation
		void printState();
		// Returns small disks concatenated as a string
		std::string *asString();

        // Get
        Circular_Array get_small_disks() { return *small_disks; }
        unsigned short get_eval() const { return evaluation; }
		unsigned short get_heuristic() { return heuristic; }
		unsigned short get_cost() { return cost; }
		State* get_parent() { return parent; }

		// Set
		void set_heuristic(unsigned short new_heuristic) { heuristic = new_heuristic; }
		void set_cost(unsigned short new_cost) { cost = new_cost; }
		void set_evaluation(unsigned short new_evaluation) { evaluation = new_evaluation; }
		void set_parent(State* new_parent) { parent = new_parent; }

    private:
        State* parent;
        Circular_Array* small_disks;
        unsigned short heuristic, cost, evaluation;
};

#endif // STATE_H
