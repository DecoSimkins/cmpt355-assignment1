#ifndef FRONTIER_MANAGER_H
#define FRONTIER_MANAGER_H

#include <unordered_map>
#include <vector>
#include <queue>

#include "State.h"
#include "state_queue.h"


class Frontier_Manager
{
    public:
		/* Initialize the frontier and list of big disks as well as allocate a new
		 * unordered map for explored nodes
 		 */
        Frontier_Manager(state_queue* init_frontier, Circular_Array* init_big_disks);
        virtual ~Frontier_Manager();

        /* Given a state s, generates all possible successor states and inserts
 		 * them into the frontier after setting their parent, cost, heuristic, and
 		 * evaluation
 		 */
        void generate_states(State* s);

		// Adds state toClose to the closed list
		void close(State* toClose);

    private:
        state_queue* frontier;
        Circular_Array* big_disks;
        std::unordered_map <std::string, State*> *closed;

		// Returns the index of the big disk which has no small disk on it in state s
        int find_empty_disk_index(State s);
		
		// Generates all indeces of valid moves given the index of the empty big disk
        std::vector <int>* gen_valid_indeces(int empty_index);
};

#endif // FRONTIER_MANAGER_H
